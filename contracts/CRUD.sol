pragma solidity >=0.4.22 <0.6.0;
pragma experimental ABIEncoderV2;

contract CRUD {
    address public owner;

    struct KeyValue {
        string value;
        uint256 listPointer;
        bool valid;
    }

    mapping(string => KeyValue) public values;
    string[] public keys;

    constructor() public {
        owner = msg.sender;
    }

    event ValueEvent(string key, string value, uint256 listPointer);

    function keyExists(string memory key) private view returns (bool) {
        if (keys.length == 0) {
            return false;
        }

        return values[key].valid;
    }

    function create(string memory key, string memory value) public {
        if (keyExists(key)) {
            revert("Key already exists");
        }

        uint256 listPointer = keys.push(key) - 1;
        values[key] = KeyValue({
            value: value,
            listPointer: listPointer,
            valid: true
        });

        emit ValueEvent(key, values[key].value, values[key].listPointer);
    }

    function read(string memory key) public view returns (string memory) {
        if (!keyExists(key)) {
            revert("Key does not exist");
        }

        return values[key].value;
    }

    function update(string memory key, string memory value) public {
        if (!keyExists(key)) {
            revert("Key does not exist");
        }

        values[key].value = value;
        emit ValueEvent(key, values[key].value, values[key].listPointer);
    }

    function destroy(string memory key) public {
        if (!keyExists(key)) {
            revert("Key does not exist");
        }

        uint256 rowToDelete = values[key].listPointer;
        string memory lastEntryToReplaceRemovedKey = keys[keys.length - 1];
        keys[rowToDelete] = lastEntryToReplaceRemovedKey;
        values[lastEntryToReplaceRemovedKey].listPointer = rowToDelete;
        keys.length--;

        values[key].valid = false;
    }

    function list() public view returns (string[] memory, string[] memory) {
        string[] memory valueList = new string[](keys.length);
        for (uint256 i = 0; i < keys.length; i++) {
            valueList[i] = values[keys[i]].value;
        }
        return (keys, valueList);
    }
}
