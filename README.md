# Setup Project

```shell
yarn init
yarn add truffle
yarn add --dev nodemon
npx truffle init
```


# Develop

```shell
KEYSTORE=./keystore PASSCODE=secret yarn web3:testnet 
yarn develop
```


# Deploy

```shell
yarn deploy
```
