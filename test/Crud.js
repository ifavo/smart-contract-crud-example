const Contract = artifacts.require('CRUD')

contract('CRUD', accounts => {
  const [firstAccount] = accounts

  describe('generics', () => {

    it('has a contract length <24kb', async () => {
      const contractInstance = await Contract.new()
      expect(contractInstance.constructor._json.deployedBytecode.length).to.be.lessThan(24000)
    })


    it('sets the creator as owner', async () => {
      const contractInstance = await Contract.new()
      const owner = await contractInstance.owner.call()
      expect(owner).to.equal(firstAccount)
    })

  })

  describe('create', () => {


    it('can create a new dataset with key/value', async () => {
      const randomValue = String(Math.random())
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      const { logs } = await contractInstance.create(randomKey, randomValue)
      expect(logs).to.have.lengthOf(1)
      expect(logs[0].args.key).to.equal(randomKey)
      expect(logs[0].args.value).to.equal(randomValue)
    })

    it('fails to create a new dataset if a key already exists', async () => {
      const randomValue = String(Math.random())
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      await contractInstance.create(randomKey, randomValue)

      try {
        const { logs } = await contractInstance.create(randomKey, randomValue)
        expect(logs).to.have.lengthOf(0)
        assert.fail()
      }
      catch (err) {
        assert.ok(/Key already exists/.test(err.message))
      }
    })

  })

  describe('read', () => {
    it('can read a value of a known key', async () => {
      const randomValue = String(Math.random())
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      await contractInstance.create(randomKey, randomValue)
      const result = await contractInstance.read(randomKey)
      expect(result).to.equal(randomValue)
    })


    it('fails to read a value of an unknown key', async () => {
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      try {
        await contractInstance.read(randomKey)
        assert.fail()
      }
      catch (err) {
        assert.ok(/Key does not exist/.test(err.message))
      }
    })
  })

  describe('update', () => {
    it('can update a value of a known key', async () => {
      const randomValue = String(Math.random())
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      await contractInstance.create(randomKey, +(new Date()))
      const { logs } = await contractInstance.update(randomKey, randomValue)

      expect(logs).to.have.lengthOf(1)
      expect(logs[0].args.key).to.equal(randomKey)
      expect(logs[0].args.value).to.equal(randomValue)
    })


    it('fails to update a value of an unknown key', async () => {
      const randomKey = String(Math.random())
      const randomValue = String(Math.random())

      const contractInstance = await Contract.new()
      try {
        await contractInstance.update(randomKey, randomValue)
        assert.fail()
      }
      catch (err) {
        assert.ok(/Key does not exist/.test(err.message))
      }
    })
  })

  describe('destroy', () => {
    it('can remove known key', async () => {
      const randomValue = String(Math.random())
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      await contractInstance.create(randomKey, randomValue)
      await contractInstance.destroy(randomKey)

      try {
        await contractInstance.read(randomKey)
        assert.fail()
      }
      catch (err) {
        assert.ok(/Key does not exist/.test(err.message))
      }
    })


    it('fails to remove an unknown key', async () => {
      const randomKey = String(Math.random())

      const contractInstance = await Contract.new()
      try {
        await contractInstance.destroy(randomKey)
        assert.fail()
      }
      catch (err) {
        assert.ok(/Key does not exist/.test(err.message))
      }
    })
  })

  describe('list', () => {
    it('returns an empty list by default', async () => {
      const contractInstance = await Contract.new()
      const { 0: keys, 1: values } = await contractInstance.list()
      expect(keys).to.have.lengthOf(0)
      expect(values).to.have.lengthOf(0)
    })

    it('can list created values', async () => {
      const randomKey = String(Math.random())
      const randomValue = String(Math.random())

      const contractInstance = await Contract.new()
      await contractInstance.create(randomKey, randomValue)

      const { 0: keys, 1: values } = await contractInstance.list()
      expect(keys).to.have.lengthOf(1)
      expect(values).to.have.lengthOf(1)

      expect(keys[0]).to.equal(randomKey)
      expect(values[0]).to.equal(randomValue)
    })

    it('does not list destroyed values', async () => {
      const randomKey = String(Math.random())
      const randomValue = String(Math.random())

      const contractInstance = await Contract.new()
      await contractInstance.create(randomKey, randomValue)
      await contractInstance.destroy(randomKey)

      const { 0: keys, 1: values } = await contractInstance.list()
      expect(keys).to.have.lengthOf(0)
      expect(values).to.have.lengthOf(0)
    })

  })
})
